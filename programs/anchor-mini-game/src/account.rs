use anchor_lang::prelude::*;

#[account]
pub struct MiniGameAccount {
    pub bump: u8,
    pub authority_pubkey: Pubkey,
    pub ticket_price: u64,
    pub ticket_sold: u64,
    pub reward_mint: Pubkey,
    pub vault_rewarding: Pubkey,
    pub max_valid_day: u64,
    pub max_reward_per_day: u64,
}

impl Space for MiniGameAccount {
    const INIT_SPACE: usize = 8 + 1 + 32 + 8 + 8 + 32 + 32 + 8 + 8;
}

#[account]
pub struct TicketAccount {
    pub bump: u8,
    pub owner_pubkey: Pubkey,
    pub buy_at: i64,
    pub reward_earned: u64
}


impl Space for TicketAccount {
    const INIT_SPACE: usize = 8 + 1 + 32 + 8 + 8;
}