use anchor_lang::prelude::*;
mod contexts;
use contexts::*;
mod account;
mod helper;
use helper::*;


declare_id!("7nJMUvunM46G4SfJBM45jDZQbkbKEQQbXCf5KVRAVTyU");

#[program]
pub mod anchor_mini_game {
    use super::*;

    pub fn initialize_mini_game(ctx: Context<Initialize> , ticket_price: u64, max_valid_day: u64, max_reward_per_day: u64 ) -> Result<()> {
        ctx.accounts.initialize_mini_game(&ctx.bumps, ticket_price, max_valid_day, max_reward_per_day)?;
        Ok(())
    }

    pub fn buy_ticket(ctx: Context<BuyTicket>) -> Result<()> {
        let ticket_price = ctx.accounts.mini_game_account.ticket_price;

        let user = &mut ctx.accounts.buyer;
        let mini_game = &mut ctx.accounts.mini_game_account;

        // //send SOL to authority 
        send_lamports(user.to_account_info(), mini_game.to_account_info(), ticket_price)?;
        ctx.accounts.save_ticket(&ctx.bumps,)?;
        Ok(())
    }

    pub fn withdraw_reward(ctx: Context<Initialize> , ticket_price: u64, max_valid_day: u64, max_reward_per_day: u64 ) -> Result<()> {
        ctx.accounts.initialize_mini_game(&ctx.bumps, ticket_price, max_valid_day, max_reward_per_day)?;
        Ok(())
    }
}