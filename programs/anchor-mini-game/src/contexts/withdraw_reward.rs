use anchor_spl::{
    associated_token::AssociatedToken,
    token::{Mint, Token, TokenAccount, Transfer, transfer},
};
use anchor_lang::prelude::*;

use crate::account::{MiniGameAccount, TicketAccount};


#[derive(Accounts)]
pub struct WithdrawReward<'info> {
    #[account(mut)]
    pub ticket_owner: Signer<'info>,
    #[account(
        mut, 
        constraint = ticket_account.owner_pubkey == ticket_owner.key(),
    )]
    pub ticket_account: Account<'info, TicketAccount>,
    pub reward_mint: Account<'info, Mint>,
    #[account(
        mut,
        constraint = mini_game_account.reward_mint == reward_mint.key(),

    )]
    pub mini_game_account: Account<'info, MiniGameAccount>,
    #[account(
        mut,       
        associated_token::mint = reward_mint,
        associated_token::authority = mini_game_account
    )]
    pub vault_rewarding: Account<'info, TokenAccount>,
    #[account(
        init_if_needed,
        payer = ticket_owner,
        associated_token::mint = reward_mint,
        associated_token::authority = ticket_owner
    )]
    pub ticket_owner_token_account: Account<'info, TokenAccount>,
    pub clock: Sysvar<'info, Clock>,
    pub associated_token_program: Program<'info, AssociatedToken>,
    pub token_program: Program<'info, Token>,
    pub system_program: Program<'info, System>,
}

impl<'info> WithdrawReward<'info> {
    pub fn send_tokens_from_vault_to_user(&self, tokens_amount: u64) -> Result<()> {
        let seeds = &[
            b"mini_game",
            self.mini_game_account.authority_pubkey.as_ref(),
            &[self.mini_game_account.bump]
        ];

        transfer(
            CpiContext::new_with_signer(
                self.token_program.to_account_info(),
                Transfer {
                    from: self.reward_mint.to_account_info(),
                    to: self.vault_rewarding.to_account_info(),
                    authority: self.ticket_owner.to_account_info(),
                },
                &[&seeds[..]]
            ),
            tokens_amount.into()
        )
    }
}
