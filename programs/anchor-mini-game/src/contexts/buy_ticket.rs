use anchor_spl::{
    associated_token::AssociatedToken,
    token::{Token},
};
use anchor_lang::prelude::*;

use crate::account::{MiniGameAccount, TicketAccount};


#[derive(Accounts)]
pub struct BuyTicket<'info> {
    #[account(mut)]
    pub buyer: Signer<'info>,
    #[account(
        init_if_needed,
        payer = buyer,
        space = TicketAccount::INIT_SPACE,
        seeds = [b"ticket".as_ref(), &buyer.key().as_ref()],
        bump
    )]
    pub ticket_account: Account<'info, TicketAccount>,
    #[account(mut)]
    pub mini_game_account: Account<'info, MiniGameAccount>,
    pub clock: Sysvar<'info, Clock>,
    pub associated_token_program: Program<'info, AssociatedToken>,
    pub token_program: Program<'info, Token>,
    pub system_program: Program<'info, System>,
}

impl<'info> BuyTicket<'info> {
    pub fn save_ticket(
        &mut self,
        bumps: &BuyTicketBumps,
    ) -> Result<()> {
        self.ticket_account.set_inner(TicketAccount {
            bump: bumps.ticket_account,
            owner_pubkey: self.buyer.key(),
            buy_at: self.clock.unix_timestamp,
            reward_earned: 0,
        });
        Ok(())
    }
}