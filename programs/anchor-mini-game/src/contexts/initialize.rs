use anchor_spl::{
    associated_token::AssociatedToken,
    token::{Mint, Token, TokenAccount},
};
use anchor_lang::prelude::*;

use crate::account::MiniGameAccount;

#[derive(Accounts)]
#[instruction(ticket_price: u64)]
pub struct Initialize<'info> {
    #[account(mut)]
    pub authority: Signer<'info>,
    pub reward_mint: Account<'info, Mint>,
    #[account(
        init_if_needed,
        payer = authority,
        space = MiniGameAccount::INIT_SPACE,
        seeds = [b"mini_game".as_ref(), &authority.key().as_ref()],
        bump
    )]
    pub mini_game_account: Account<'info, MiniGameAccount>,
    #[account(
        init_if_needed,
        payer = authority,
        associated_token::mint = reward_mint,
        associated_token::authority = mini_game_account
    )]
    pub vault_rewarding: Account<'info, TokenAccount>,
    pub associated_token_program: Program<'info, AssociatedToken>,
    pub token_program: Program<'info, Token>,
    pub system_program: Program<'info, System>,
}

impl<'info> Initialize<'info> {
    pub fn initialize_mini_game(
        &mut self,
        bumps: &InitializeBumps,
        ticket_price: u64,
        max_valid_day: u64,
        max_reward_per_day: u64
    ) -> Result<()> {
        self.mini_game_account.set_inner(MiniGameAccount {
            bump: bumps.mini_game_account,
            authority_pubkey: self.authority.key(),
            ticket_price,
            reward_mint: self.reward_mint.key(),
            vault_rewarding: self.vault_rewarding.key(),
            ticket_sold: 0,
            max_valid_day,
            max_reward_per_day
        });
        Ok(())
    }
}