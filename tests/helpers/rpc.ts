import { PublicKey, Signer, SystemProgram, LAMPORTS_PER_SOL } from "@solana/web3.js";
import {
    ASSOCIATED_TOKEN_PROGRAM_ID,
    getOrCreateAssociatedTokenAccount,
    TOKEN_PROGRAM_ID,
    getAssociatedTokenAddress,
} from "@solana/spl-token";
import * as anchor from "@coral-xyz/anchor";
import { Ctx } from "./ctx";
import bs58 from 'bs58';

const TICKET_PRICE = new anchor.BN(0.15 * LAMPORTS_PER_SOL);//0.15 SOL
const MAX_VALID_DAY = new anchor.BN(2592000); //30 DAYS
const MAX_REWARD_PER_DAY = new anchor.BN(20_000); // 20.000 token per day

export namespace RPC {
    export async function initializeMiniGame(ctx: Ctx) {
        await ctx.program.methods.initializeMiniGame(
            TICKET_PRICE, 
            MAX_VALID_DAY,
            MAX_REWARD_PER_DAY
        ).accounts({
            authority: ctx.authority.publicKey,
            rewardMint: ctx.rewardMint,
            miniGameAccount: ctx.miniGameAccount,
            vaultRewarding: ctx.vaultRewarding,
            associatedTokenProgram: ASSOCIATED_TOKEN_PROGRAM_ID,
            tokenProgram: TOKEN_PROGRAM_ID,
            systemProgram: SystemProgram.programId,
        }).signers([ctx.authority]).rpc();
    }

    export async function buyTicket(ctx: Ctx) {
        await ctx.program.methods.buyTicket().accounts({
            miniGameAccount: ctx.miniGameAccount,
            buyer: ctx.buyer.publicKey,
            ticketAccount: ctx.ticketAccount,
            clock: anchor.web3.SYSVAR_CLOCK_PUBKEY,
            associatedTokenProgram: ASSOCIATED_TOKEN_PROGRAM_ID,
            tokenProgram: TOKEN_PROGRAM_ID,
            systemProgram: SystemProgram.programId,
        }).signers([ctx.buyer]).rpc();
    }

}

export interface MiniGameData {
    address: PublicKey,
    bump: number,
    tokenVault: PublicKey,
    owner: PublicKey,
    tokenPrice: anchor.BN,
    tokenAmount: { tokens: anchor.BN },
}
