import {
    PublicKey,
    Connection,
    Signer,
    LAMPORTS_PER_SOL,
} from '@solana/web3.js';
import {
    createMint,
    getOrCreateAssociatedTokenAccount,
    Account as TokenAccount,
    mintTo,
    getAssociatedTokenAddress,
} from '@solana/spl-token';
import { Program } from "@coral-xyz/anchor";
import { AnchorMiniGame } from "../../target/types/anchor_mini_game";
import * as anchor from "@coral-xyz/anchor";
import { createUserWithLamports } from './helpers';

// This interface is passed to every RPC test functions
export interface Ctx {
    connection: Connection,
    program: Program<AnchorMiniGame>,
    // The authority of mini game
    authority: Signer,
    buyer: Signer,
    // The mint of the tokens that reward to player
    rewardMint: PublicKey,
    // minigame ATA for storing the reward tokens
    vaultRewarding: PublicKey,
    miniGameAccount: PublicKey,
    ticketAccount: PublicKey,

}

export async function createCtx(connection: Connection, program: Program<AnchorMiniGame>): Promise<Ctx> {
    const authority = await createUserWithLamports(connection, 1);
    const rewardMint = await createMint(
        connection,
        authority, // payer
        authority.publicKey, // mintAuthority
        authority.publicKey, // freezeAuthority
        6 // decimals
    );
    const tokensForReward = await getOrCreateAssociatedTokenAccount(connection, authority, rewardMint, authority.publicKey);
    const tokensForRewardAmount = 10_000_000;
    await mintTo(
        connection,
        authority,
        rewardMint,
        tokensForReward.address,
        authority,
        tokensForRewardAmount,
    );
    const miniGameAccount = PublicKey.findProgramAddressSync(
        [Buffer.from("mini_game"), authority.publicKey.toBuffer()],
        program.programId
      )[0];
    
    //token account for rewarding user
    const vaultRewarding = await getAssociatedTokenAddress(rewardMint, miniGameAccount, true);
    
    const buyer = await createUserWithLamports(connection, 1);

    const ticketAccount = PublicKey.findProgramAddressSync([
        Buffer.from("ticket"), buyer.publicKey.toBuffer()],
        program.programId
    )[0];

    return {
        connection,
        program,
        authority,
        buyer,
        rewardMint,
        vaultRewarding,
        miniGameAccount,
        ticketAccount
    }
}


