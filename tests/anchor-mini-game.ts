import * as anchor from "@coral-xyz/anchor";
import { Program } from "@coral-xyz/anchor";
import { AnchorMiniGame } from "../target/types/anchor_mini_game";
import { Connection, LAMPORTS_PER_SOL } from '@solana/web3.js';
import { Ctx, createCtx } from "./helpers/ctx";
import { RPC } from "./helpers/rpc";
import { CheckCtx } from "./helpers/check";

describe("anchor-mini-game", () => {
  anchor.setProvider(anchor.AnchorProvider.env());

  const program = anchor.workspace.AnchorMiniGame as Program<AnchorMiniGame>;
  const connection = new Connection("http://localhost:8899", 'recent');
  let ctx: Ctx;

  it("Initialized minigame", async () => {
    ctx = await createCtx(connection, program);
    await RPC.initializeMiniGame(ctx);
    await CheckCtx.miniGameInfo(ctx);
  });

  it("Buy ticket", async () => {

    const miniGameBalanceBefore = (await connection.getAccountInfo(ctx.miniGameAccount)).lamports;
    await RPC.buyTicket(ctx);

    await CheckCtx.checkTicket(ctx);
    await CheckCtx.lamportsBalance(ctx, ctx.miniGameAccount, miniGameBalanceBefore, new anchor.BN(0.15 * LAMPORTS_PER_SOL));

  });
});

